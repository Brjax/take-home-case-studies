
from pathlib import Path
import csv


def line_parser(line):
    try:
        line = line.replace(',', '').strip().split('"')
        line = [x for x in line if x != '']
        # for the ": {" following lastOpenedAt/lastLiveAt/location
        if line[1] == ': {':
            return line[0], '{'
        # used to remove ":" when "latitude": 21.3878362 becomes ': 21.3878362' while parsing
        if line[0] in ['latitude', 'longitude']:
            return line[0], float(line[-1][1:])
        # all other key, value pairs
        k, v = line[0], line[-1]
    except Exception as e:
        print("Parsing error: " + str(e))
        return None
    return k, v


p = Path.cwd() / 'data'
json_file = p / 'results.json'

#  only the following columns are needed
cols = ['lastopenedat', 'objectid', 'appname', 'email', 'localeidentifier', 'devicetype', 'timezone',
        'installationid', 'devicetoken', 'latitude', 'longitude']

nest = list()  # stack to track the depth of nested objects
row_val = dict()  # dictionary to hold column values
last_op = False  # track if open bracket follows lastOpenedAt to pull correct iso value
with open(json_file, 'r') as json, open(p / 'res_out.csv', 'w') as csv_f:
    writer = csv.writer(csv_f)
    writer.writerow(cols)
    #  iterate through items in file
    for item in json:
        # results corresponds to the outermost object
        if item.strip() in ['{', '"results": [']:
            #  used to track balance of brackets to determine depth of nested objects
            nest.append('{')
        elif item.strip() in ['}', '},', ']']:  # '},'follows lastOpenedAt/lastLiveAt
            __ = nest.pop()
            #  when nest equal 2, one full row has been parsed
            if len(nest) == 2:
                row = []
                for col in cols:
                    row.append(row_val.get(col, "null"))
                try:
                    writer.writerow(row)
                except Exception as e:
                    print("Error on write: " + str(e))
                row_val.clear()
            if len(nest) == 0:
                print("Conversion Complete")
                break
        else:
            key, val = line_parser(item)
            if val == '{':
                nest.append('{')
            if key == 'lastOpenedAt':
                last_op = True
            elif key == 'iso':
                if last_op:
                    row_val['lastopenedat'] = val
                    last_op = False
            elif key.lower() in cols:
                row_val[key.lower()] = val
    csv_f.close()
